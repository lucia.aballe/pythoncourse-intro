#!/usr/bin/env python


# Solutions to scipy course
# By carlos.pascual@cells.es

"""
Exercise: matplot1
------------------

Do a script that reads the "converted1.dat" created in the converter1
exercise and plots counts VS time.

Once you got it, change the Y axis to log scale.
Add titles for the axes in the plot

Tips:

- See http://matplotlib.sourceforge.net/users/pyplot_tutorial.html
- For semilog plots you can use matplotlib.pyplot.semilogy()
- [Official Solution](exercises/matplot1.py)
"""

# Write your solution here

import matplotlib as mp
import matplotlib.pyplot as pp
import numpy as np

data = np.loadtxt('converted1.dat')
# print(data.shape)
x_values = data[:, 0]
y_values = data[:, 1]

# pp.figure(1)
# necessary for both to stay open, but does not change output after introducing saving: both close at the end anyway
# fig1 = pp.figure() # works apparently the same
pp.title('Behold! Linear plot for converted1!')
pp.plot(x_values, y_values)
pp.ylabel('normalized intensity')
pp.xlabel('time [ps]')
pp.show(block=False)

pp.savefig('mp1.png')
# pp.close()              # not working for me

# pp.subplot(212)
# pp.figure(2)          # does not change output after introducing saving
# fig2 = pp.figure()   # works apparently the same
pp.title('Behold! Semi-log plot for converted1!')
pp.semilogy(x_values, y_values)
pp.ylabel('normalized intensity')
pp.xlabel('time [ps]')
pp.show(block=False)

pp.savefig('mp2.png')

# Without the final saving step I do not see the figs
# Without it, both are there but script remains in 'running' mode.

